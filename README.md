# Пример CI/CD

Данный проект служит для демонстрации простого CI/CD. Пайплайн берёт приложение, тестирует его , 
собирает и пушит докер имэдж на Докер Хаб и деплоит на удалённый сервер.

## Содержание
* [Приложение](#приложение)
* [Стэйджи](#стэйджи)
  * [Тест](#тест)
  * [Сборка](#сборка)
  * [Деплой](#деплой)
* [Результат](#результат)

## Приложение

Для демонстрации используется простое приложение на Питоне для обработки строк. Оно берёт строку и реверсирует её
и проверяет наличие в строке указаных символов.

## Стэйджи

Пайплайн состоит из трёх стадий - тест , сборка и деплой

### Тест

Запускаются два теста на встроеноом в пайтон модуле unittest. джоб не запускается при изменениях в файлах README.md и .gitlab-ci.yml

```
test:
  image: python:3.8.13
  stage: test
  before_script:
    - pip install --upgrade pip
    - pip install pyinstaller
    - pyinstaller --version
  script:
    - python test_rev.py
    - python test_seek.py
  rules:
    - if:
      changes: 
        - README.md
        - .gitlab-ci.yml
      when: never

```

![pythonTest](./Images/Python_test.png)


### Сборка

После прохождения тестов собирается докер имэдж и пушится на Докер Хаб

```
build:
  stage: build
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  before_script:
    - docker login -u $DOCKER_USER -p $DOCKER_PASSWORD
  script:
    - docker build -t $IMAGE_NAME:$IMAGE_TAG .
    - docker push $IMAGE_NAME:$IMAGE_TAG

```
![DockerHub](./Images/DockerHub.png)

### Деплой 

Данный джоб запускается в ручную при необходимости задеплоить приложение на удалённый сервер. В примере используется 
локальный хост 192.168.1.105

```
deploy:
  stage: deploy
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  before_script:
    - chmod 400 $SSH_KEY
  script:
    - ssh -o StrictHostKeyChecking=no -i $SSH_KEY root@192.168.1.105 "
        docker login -u $DOCKER_USER -p $DOCKER_PASSWORD &&
        docker ps -aq | xargs docker stop | xargs docker rm || echo "No Images" &&
        docker run -di $IMAGE_NAME:$IMAGE_TAG"

```
Так же создаётся артифакт

```
  script:
   - mkdir ./artifacts/
    - echo 'success!!' >> artifacts/build
  artifacts:
    paths:
      - artifacts/build
    expire_in: 7 days
  when: manual


```
## Результат

После выполнения пайплайна можно зайти на хост и убедиться, что всё работает

![Results](./Images/Results.png)
