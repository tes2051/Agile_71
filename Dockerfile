FROM python:3.8.13

WORKDIR /usr/src/app

COPY ./Agile_7.py /usr/src/app

CMD "python"
